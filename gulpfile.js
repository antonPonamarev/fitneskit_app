var gulp = require('gulp');

var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var Export = require('gulp-export');
var babel = require('gulp-babel');
var react = require('gulp-react');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('html', function(){
  return gulp.src('src/*.html')
    .pipe(concat('index.html'))
    .pipe(gulp.dest('build/'))
});


gulp.task('css', function(){
  return gulp.src('style/*.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('public/css'))
});

gulp.task('js', function(){
  return gulp.src('debug/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
      }))
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js/'))
});

gulp.task('es6',['js'], () =>
  gulp.src('debug/_js/*.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('public/js/'))
);


gulp.task('watch',function(){
 watch('*.html',function(ev,callback){
    gulp.start('html')
  })
  watch('style/*.less',function(ev,callback){
    gulp.start('css')
  })
  watch('js/*.js',function(ev,callback){
    gulp.start('js')
  })
})
gulp.task('build', [ 'js', 'es6' ]);
gulp.task('default', [ 'html', 'css', 'js' ]);