//ПОСТРОЕНИЕ ИНТЕРФЕЙСА
function builder(config){
    var d = document, c = "click";
    var num = 0,m = 0;
    //ДОБАВЛЯЕТ ФИЛЬТР ОСНОВУ ОДНОГО ИЗ ФИЛЬТРОВ 
    //ПОЛУЧАЕТ СПИСОК ДЛЯ ВЫПАДАЮЩЕГО СПИСКА, ИМЯ ФИЛЬТРА И ЕГО КОДОВОЕ НАЗВАНИЕ
    function buildFilterCol(items,name,prefix){
        //ПЕРЕМЕНЫЕ КОТОРЫЕ БУДУТ ПОДСТАВЛЯТЬСЯ В ВЕРСТКУ
         let prefixObj = {
            "opt":{
                "name":name,
                "filterList":items.map(buildFilterList).toString().replaceAll(",",""),
                "prefix":prefix
            }
        }
        
        $(".fs-schedule-wg-filter").append(customHtmlObject.fillHtml(customHtmlObject.filterCol,prefixObj));
    }
    //СТРОИТ ЭЛЕМЕНТЫ ВЫПАДАЮЩЕГО СПИСКА
    function buildFilterList(filterItem,index) {
        filterItem.image_url = filterItem.image_url === undefined? "display:none;": filterItem.image_url; 
        let prefixObj = {
            "filterItem":filterItem,
            "opt":{
                "name":filterItem.full_name!==undefined?`${filterItem.full_name}`:filterItem.name
            }
        }
        
        return customHtmlObject.fillHtml(customHtmlObject.filterItem,prefixObj);
     }
    

    //ПОСТРОЕНИЕ ЯЧЕЙКИ В ТАБЛИЦЕ
    function buildeLessonCell(lesson,index) {
        lesson.tab = (m++)%3;
         if(config.trainersAssos[lesson.coach_id] !== undefined)       
            {        
            let prefixObj = {
                "les":lesson,
                "opt":{
                    "client_recorded":lesson.client_recorded?`<i class="fas fa-phone"></i>`:"",
                    "availabile": lesson.availabile_slots!="-1"? `<span class="availabile-number" style="background:%opt-color%;">мест : ${lesson.availabile_slots}</span>` :"",
                    "ico":lesson.commercial?'<i class="fas fa-money-bill"></i>':"",
                    "color":config.tabsAssos[lesson.tab_id].color,
                    "last_name":config.trainersAssos[lesson.coach_id].full_name,
                    "index":index
                }
            }
            if(params["print"]=="Y") lesson.timeArea = 1; // для print
            $("table").find("tr:eq("+lesson.timeArea+")")
                        .find("td:eq("+lesson.weekDay+")")
                        .append(customHtmlObject.fillHtml(customHtmlObject.tableCell,prefixObj));}
        
    }
    //ПРИМЕНЯЕТСЯ ЕСЛИ В GET ПАРАМЕТРАХ ПЕРЕДАЛИ ВЫБРАНЫЕ ЗНАЧЕНИЯ ФИЛЬТРА
    function setFilter(arr,filter){
        if(arr.length>0){
            $(`.${filter}-col`).find(".fs-filter-all").removeClass("fs-filter-active-item");
            arr.forEach(function(elem){
                if(elem!="")$(`.${filter}-col`).find(".ft-list").find(`.fs-filter-item:eq(${elem})`).addClass("fs-filter-active-item");
            })
        }
        
    }
    
    
    //ФОРМИРУЕМ АССОЦИАТИВНЫЙ МАССИВ 
    function assosArr(arr){
        let tmp = {};
        arr.forEach(function(elem){
            tmp[elem.id] = elem;
        })
        return tmp;
    }
    
    //Дополняет массив номером дня недели и номером временной зоны
    function getWeekDate(arr){
        arr.map(function(elem,index){
            var dateArr = elem.date.split("-"),
            d = new Date(dateArr[0],dateArr[1]-1,dateArr[2]);
            arr[index].weekDay = (d.getDay()+6)%7+1;
            arr[index].timeArea = Math.ceil((elem.startTime.split(":")[0]))-7;
            arr[index].time = (parseInt(elem.endTime.split(":")[0])*60 +parseInt(elem.endTime.split(":")[1])) - (parseInt(elem.startTime.split(":")[0])*60 +parseInt(elem.startTime.split(":")[1]));
        })
        return arr;
    }
    
function changeCSS( color,id ) {
    let prefixObj = {opt:{color:color}};
    $("head").append(customHtmlObject.fillHtml(customHtmlObject.specialColorStyle,prefixObj));
}
    
    (function(config){
       console.log(config);
        if(config.option!==undefined){
            $("h1").text(`Расписание "${config.option.club_name}"`);
            $(".appl-android").attr("href",config.option.link_android);
            $(".appl-appstore").attr("href",config.option.link_ios);
             if(params["print"]!="Y") changeCSS(config.option.primary_color, "primaryStyle");
        }
        
        config.trainersAssos = assosArr(config.trainers);
        config.tabsAssos = assosArr(config.tabs);
    
        config.lessons = getWeekDate(config.lessons);
        
        buildFilterCol(config.trainers,"Инструкторы","teacher");
        buildFilterCol(config.tabs,"Направления","tab");
        
        config.lessons.forEach(buildeLessonCell);
        
        //ОБРАБОТКА ПЕРЕДАНЫХ ЗНАЧЕНИЙ ФИЛЬТРА
        if(params["teacher"] !== undefined)
            setFilter(params["teacher"].split("|"),"teacher");
        
        
        if(params["tab"] !== undefined)
            setFilter(params["tab"].split("|"),"tab");
        
        
        
        eventsHandler(document,config);
        $(".loader-bcg").hide();
        
        if(params["print"]=="Y") window.print() ;
        
 
    })(config)

}