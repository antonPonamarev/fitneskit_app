//ОБРАБОТЧИКИ СОБЫТИЙ 
function eventsHandler(d, config){
            let c = "click";
            //КЛИК ПО ФИЛЬТРУ(ВЫПАДАЮЩИЙ СПИСОК)
            $(d).on(c, ".fs-schedule-wg-dropdown-value", function () {
                $(this).parent().toggleClass("opened");
            })
            //КЛИК ПО ЭЛЕМЕНТУ ФИЛЬТРА
            .on(c, ".fs-filter-item:not(.fs-filter-all)", function () {
                $(this).toggleClass("fs-filter-active-item");
                let prefix = $(this).parent().data("prefix");
                clearFilter(`.${prefix}-col`);
                applyFilter(prefix);
            })
            //ЗАКРЫТИЕ ВЫПАДЮЩЕГО СПИСКА
            .on(c, function(event){
                parent.postMessage($("body").height(), '*');
                var hasParent = $(event.target).parents('.fs-schedule-wg-dropdown').length;
                if (!hasParent){
                    $('.opened').each(function(){
                        $(this).removeClass('opened');
                    });
                }
            })
            //КЛИК ПО ФИЛЬТРУ "ВСЕ"
            .on(c, ".fs-filter-all", function () {
                $(this).addClass("fs-filter-active-item");
                $(".fs-filter-item").removeClass("fs-filter-active-item");
                applyFilter();
            })
            //КЛИК ПО ДНЮ НЕДЕЛИ(МОБИЛЬНАЯ ВЕРСИЯ)
            .on(c, ".fs-schedule-wg-day", function () {
                $(".fs-schedule-wg-day").removeClass("fs-schedule-wg-day-active");
                $(this).addClass("fs-schedule-wg-day-active");
                let num = $(this).index() + 1;
                $("tr td").hide();
                $("tr td:nth-child("+num+")").show();
               
            })
            //КЛИК ПО ЗАНЯТИЮ В ТАБЛИЦЕ (ОТКРЫТИЕ ВСПЛЫВАЮЩЕГО ОКНА)
            .on(c, ".fs-schedule-cell-wrap", function () {
                if(params["print"]=="Y") return;
                
                $(".is-schedule").toggleClass("modal-open");
                $("#chain_modal").toggleClass("show");
                $("#chain_modal").show();
                
                let lessonForm = config.lessons[$(this).data("id")];
                lessonForm.description = lessonForm.description==""?"Описание отсутствует":lessonForm.description;
                
                let trenerForm = config.trainersAssos[lessonForm["coach_id"]];
                trenerForm.tabDesc = trenerForm!==undefined? `<div  class="fs-popup-tab ng-scope" >О тренере</div>`: "";
                
                
                
                let prefixObj = {
                    "les":lessonForm,
                    "trener":trenerForm,
                    "tab":config.tabsAssos[lessonForm.tab_id],
                    "opt":{
                        "availabile": lessonForm.availabile_slots!="-1"? `<span class="availabile-number" style="background:#d3222c;">мест : ${lessonForm.availabile_slots}</span>` :"",
                        "commercia":lessonForm.commercial?'<i class="fas fa-money-bill"></i> - Платное занятие':"",
                        "time":"55"
                    }
                }

                $(".modal-content").html(customHtmlObject.fillHtml(customHtmlObject.formPopup,prefixObj));
                if(topPopup>0)
                {  
                    if(($("body").height()-topPopup) < 700){
                       $("#chain_modal").css({ "position": "absolute", "bottom": 0,"top":"unset"});
                    }
                    else{
                       $("#chain_modal").css({ "position": "absolute", "top": topPopup });
                    }
                }
               
            })
            //ЗАКРЫТИЕ ОКНА
            .on(c, ".modal-close, .shadow-bg", function () {
                $(".is-schedule").toggleClass("modal-open");
                $("#chain_modal").toggleClass("show");
                $("#chain_modal").fadeToggle();
            })
            //ПЕРЕКЛЮЧЕНИЕ ОПИСАНИЕ ЗАНЯТИЯ / ОПИСАНИЕ ТРЕНЕРА
            .on(c,".fs-popup-tab",function(){
                $(".fs-popup-tab ").removeClass("fs-popup-active-tab");
                $(".fs-popup-content-tab").removeClass("fs-popup-active-content-tab");
                $(this).addClass("fs-popup-active-tab");
                $(`.fs-popup-content-tab:eq(${$(this).index()})`).addClass("fs-popup-active-content-tab");
                
            });
            
    
              window.addEventListener('message', function(event) { 
                if(event.data < 0){
                    window.topPopup = -event.data - 15;
                    parent.postMessage($("body").height(), '*');
                }
              });
    
//ОЧИСТКА ДРУГОГО ФИЛЬТРА(ДЛЯ ИСКЛЮЧЕНИЯ ПЕРЕСЕЧЕНИЙ)
function clearFilter(prefix){
    $(".fs-schedule-wg-filter-col").not(prefix).find(".fs-filter-item:not(.fs-filter-all)").removeClass("fs-filter-active-item");
    $(".fs-schedule-wg-filter-col").not(prefix).find(".fs-filter-all").addClass("fs-filter-active-item");
}

function setMobileDay(){
    $("tr td").hide();
    $("tr td:nth-child("+`${params.now.getDay()+1}`+")").show();
    $(`tr td:nth-child(${params.now.getDay()+1})`).css("background-color","#f5efd8");
}
    
//ПРИМЕНИТЬ ФИЛЬТР
function applyFilter(prefix){
    self = $(`.${prefix}-col`);
    let filterGETParams = "";

    if ( self.find(".fs-filter-active-item").length > 0){
        self.find(".fs-filter-all").removeClass("fs-filter-active-item");
        $(".fs-schedule-cell-wrap").addClass("fs-schedule-cell-wrap-hide");
    
            self.find(".fs-filter-active-item").each(function(){
                //собираем выбраные параметры фильтра, чтобы отправить их на печать
                filterGETParams +=$(this).index()+"|";
                $(`.fs-schedule-cell-wrap[data-${prefix}=${$(this).data("id")}]`).removeClass("fs-schedule-cell-wrap-hide");
            })
            updateUI();
    }
    else {
        $(".fs-schedule-cell-wrap").removeClass("fs-schedule-cell-wrap-hide");
        $(".fs-filter-all").addClass("fs-filter-active-item");
        updateUI();
    }

$(".print-button").attr("action",$(".print-button").data("action")+"&"+prefix+"="+filterGETParams); 
}
    
//ОБНОВИТЬ ИНТЕРФЕЙС С УЧЕТОМ ФИЛЬТРА
function updateUI(){
    var t = 0, m = 0;
    $("tr").removeClass("special");
    
    $("tbody tr").each(function () {
        if ($(this).find(".fs-schedule-cell-wrap:not(.fs-schedule-cell-wrap-hide)").length > 0) {
            $(this).addClass("showRow").removeClass("hideRow");
            if (t % 2 == 0) $(this).addClass("special"); 
            t++;
        } else $(this).addClass("hideRow").removeClass("showRow");
    });
    
    if($(document).find(".showRow").length == 0) $(".msg_error").show();
    else $(".msg_error").hide();  
      parent.postMessage($("body").height(), '*');
   
}
        if(params["teacher"] !== undefined)
            applyFilter("teacher");
        
        if(params["tab"] !== undefined)
            applyFilter("tab");

updateUI();
 setMobileDay();           
}