function timesHandler(){
    let month =['Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря'],
            weekArr = ["ПН","ВТ","СР","ЧТ","ПТ","СБ","ВС"];
    window.params = {}; //для хранения GET параметров 
    location.search.replace("?","").split("&").forEach(function(elem){
        let tmp = elem.split("=");
        params[tmp[0]] = tmp[1];
    })

    let startTimeStrArr,endTimeStrArr;
    let now = new Date(), d = new Date();
    
    if(params.start_date!==undefined && params.end_date!==undefined){
        startTimeStrArr = params.start_date.split("-");
        endTimeStrArr = params.end_date.split("-");
    }
    else{
        d.setDate(now.getDate()- (now.getDay()+6)%7);
        startTimeStrArr = [d.getFullYear(),d.getMonth() + 1,d.getDate()];
        d.setDate(d.getDate()+6);
        endTimeStrArr = [d.getFullYear(),d.getMonth() + 1,d.getDate()];
        params.start_date = startTimeStrArr.join("-");
        params.end_date = endTimeStrArr.join("-");
    }
    
    
        d.setDate(now.getDate()- (now.getDay()+6)%7);
        let startTimeStNow = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        d.setDate(d.getDate()+6);
        let endTimeStrNow = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");

      $(".today-btn").attr("href",`${location.pathname}?start_date=${startTimeStNow}&end_date=${endTimeStrNow}&club_id=${params.club_id}`);
    
    
    
    
    
    
    
    
    params.now  = now;
    //Заполняем в шапке метку с датами
    $(".fs-schedule-wg-period").find("span").text(`${startTimeStrArr[2]} ${month[startTimeStrArr[1]-1]} - ${endTimeStrArr[2]} ${month[endTimeStrArr[1]-1]}`);

    d = new Date(startTimeStrArr[0],startTimeStrArr[1]-1,startTimeStrArr[2]);
    //ЗАПОЛНЯЕМ ДНИ НЕДЕЛИ ДЛЯ ШАПКИ ТАБЛИЦЫ
    for(var i = 0; i<7; i++){
        let active = d.getDate() ==now.getDate() ? "fs-schedule-wg-day-active":""; 
       $(".fs-schedule-wg-days").append(`<div class="fs-schedule-wg-day ${active}">${weekArr[i]}<span class="fs-schedule-wg-day__date ng-binding">${d.getDate()}</span></div>`);
        $("thead tr").append(`
                   <th class="">
                        <div class="ng-binding">${weekArr[i]}. ${d.getDate()}/${d.getMonth()+1}</div>
                    </th>`);
        d.setDate(d.getDate()+1);
    }


    //ПОСТРОЕНИЕ ПЕРЕКЛЮЧАЛКИ НЕДЕЛЬ (КНОПКА СЛЕДУЮЩАЯ НЕДЕЛЯ)
        d.setDate((new Date(endTimeStrArr[0],endTimeStrArr[1]-1,endTimeStrArr[2])).getDate()+1); //НАЧАЛО СЛЕДУЮЩЕЙ НЕДЕЛИ
        let startTimeStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        d.setDate(d.getDate()+6); //КОНЕЦ СЛЕДУЮЩЕЙ НЕДЕЛИ
        let endTimeStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        //СТРОИМ ССЫЛКУ
        $(".next-week").attr("href",`${location.pathname}?start_date=${startTimeStr}&end_date=${endTimeStr}&club_id=${params.club_id}`);


    let typeWeek = (getDayDelta([startTimeStrArr[0], startTimeStrArr[1]-1,startTimeStrArr[2]], [now.getFullYear(),now.getMonth(),now.getDate()])>0);




    //ПОСТРОЕНИЕ ПЕРЕКЛЮЧАЛКИ НЕДЕЛЬ (КНОПКА ПРЕДЫДУЩАЯ НЕДЕЛЯ)
        if(typeWeek){
        d = new Date(startTimeStrArr[0],startTimeStrArr[1]-1,startTimeStrArr[2]-1);
        endTimeStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        d.setDate(d.getDate()-6);
        startTimeStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        $(".prev-week").attr("href",`${location.pathname}?start_date=${startTimeStr}&end_date=${endTimeStr}&club_id=${params.club_id}`);
                    $(".prev-week").show()
        }
        else{
            $(".prev-week").hide();
        }

}