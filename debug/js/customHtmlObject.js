//СОДЕРЖИТ HTML ШАБЛОНЫ 
//И МЕТОД fillHtml, КОТОРЫЙ ЗАПОЛНЯЕТ ВЫБРАНЫЙ ШАБЛОН
//fillHtml ПРИНИМАЕТ ДВА ПАРАМЕТРА : ШАБЛОН И ОБЪЕКТ С ПЕРЕМЕННЫМИ ДЛЯ ПОДСТАНОВКИ 
//ПЕРЕМЕННЫЕ ИМЕЮТ ВИД %ИМЯ_ОБЪЕКТА-ИМЯ_ПОЛЯ%
//ДЛЯ ПЕРЕДАЧИ ДОПОЛНИТЕЛЬНЫХ ПЕРЕМЕННЫХ ИСПОЛЬЗУЮТ ОБЪЕКТ opt
(function(w){w.customHtmlObject = {
    "formPopup" : `
    <div class="modal-content">
            <div class="fs-modal-header">
                    <span class="modal-close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></span>
                <div class="modal-title" id="orderLabel">
                    <div class="fs-modal-chain-title">
                        <div class="ng-binding ng-scope fs-les-name">%les-name% %opt-availabile%</div>
                    </div>
                    <div class="fs-modal-chain-time ng-binding">
                        <i class="fa fa-clock-o"></i>
                        <span class="fs-les-startTime"></span>
                    </div>
                    <div class="fs-clear"></div>
                </div>
            </div>
            <div class="fs-modal-desc">
                <div class="fs-modal-info ng-binding">
                    <span className="fs-tab-name"> %tab-name%</span><br>
                    Продолжительность: %les-time% мин. <br> %opt-commercia% <br>
                </div>
                <div class="fs-modal-info">
                </div>
            </div>

            <div class="fs-coach-block">
                    <span class="fs-schedule-wg-popup-coach-img">
                        <span class="fs-trainer-photo-container ng-scope"  style="background-image: url(%trener-image_url%);"></span>
                    </span>
                <span class="fs-schedule-wg-popup-coach-txt">
                        <div class="fs-schedule-wg-popup-coach-name ng-scope">
                            <span  class="ng-binding ng-scope fs-trener-name"> %trener-full_name%</span>
                        </div>
                        <div class="fs-schedule-wg-popup-room ng-binding ng-scope">
                            <i class="fas fa-user"></i>
                            <span class="fs-trener-position">%trener-posistion%</span>
                        </div>

                    </span>
                <div class="fs-clear"></div>

                <div class="fs-popup-tabs ng-scope">
                    <div  class="fs-popup-tab ng-scope fs-popup-active-tab" >О занятии
                    </div>
                     %trener-tabDesc%
                    <div class="fs-clear"></div>
                </div>

                <div class="fs-popup-tabs-content ng-scope">
                    <div class="fs-popup-content-tab ng-binding fs-popup-active-content-tab fs-les-description">%les-description%
                    </div>
                    <div class="fs-popup-content-tab fs-les-description">%trener-description%
                    </div>
                </div>
            </div>

        </div>
`,
"tableCell" :  `
<div class="fs-schedule-cell-wrap" data-id="%opt-index%" data-teacher = "%les-coach_id%" data-tab="%les-tab_id%">
            <chain  class="ng-isolate-scope">
                <div class="fs-schedule-cell" style="border-bottom: 3px solid %les-color%;">
                    <div class="fs-schedule-wg-cell-title">
                        <div class="ng-binding"> %opt-ico% %les-name% </div>
                    </div>
                    <div class="fs-schedule-wg-cell-time ng-binding"> %opt-client_recorded% %les-startTime% - %les-endTime%</div>
                    <div class="fs-schedule-wg-cell-descr">%les-place% %opt-availabile%</div>
                    <div class="fs-schedule-wg-cell-train">
                        <div class="ng-binding ">%opt-last_name%</div>
                    </div>
                </div>
            </chain>
        </div>
`,
"filterItem":`
    <div class="fs-filter-item" data-id ="%filterItem-id%"> <img class="fs-trainer-photo" alt="" src= "%filterItem-image_url%" style="%filterItem-image_url%">
                <p class="fs-trainer-name">%opt-name%</p>
                <span class="ico"> <i class="fas fa-check"></i></span>    
                <div class="fs-clear"> </div>
    </div>
`,
"filterCol": `
            <div class="fs-schedule-wg-filter-col %opt-prefix%-col" >
                <div class="fs-schedule-wg-dropdown">
                    <div class="fs-schedule-wg-dropdown-value">%opt-name%</div>
                        <div class="fs-schedule-wg-dropdown-options">
                            <div class="fs-filter-item fs-filter-all">Все <span class="ico"> <i class="fas fa-check"></i></span>  </div>
                                <div>
                                    <div class="ft-list" data-prefix="%opt-prefix%"> %opt-filterList%
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
`,
"specialColorStyle":`
<style type="text/css" id="primaryStyle">      .fs-schedule-wg-day, .fs-modal-chain-title, .fs-trener-name,.fs-schedule-wg-period a.btn{
           color: %opt-color%;
       }
       .fs-schedule-wg-today-button.btn, .fs-schedule-wg-print-button.btn,.fs-schedule-wg_reg .btn,.fs-popup-active-tab,.fs-schedule-wg-day-active{
           background-color: %opt-color%;
       }
        .fs-schedule-wg-day-active{
color:#fff
}
       .fs-popup-tabs,.fs-modal-header{
           border-color: %opt-color%;
       }</style>
`,
"fillHtml" : function(template,contentArr){

    let templateElem = template;
    
    for(var key in contentArr)
        for(var item in contentArr[key])
            templateElem = templateElem.replaceAll(`%${key}-${item}%`,contentArr[key][item]);

    return templateElem;
    
}
}})(window)
function getDayDelta(
    incomingDate, //новая дата
    todayDate //текущая дата
){
  var incomingDate = new Date(incomingDate[0],incomingDate[1]-1,incomingDate[2]),
      today = new Date(todayDate[0], todayDate[1]-1, todayDate[2]), delta;
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);

  delta = incomingDate - today;

  return Math.round(delta / 1000 / 60 / 60/ 24);
}