var XD = function () {

    var interval_id,
        last_hash,
        attached_callback,
        window = this;

    return {
        getPosition: function (el) {
            var xPos = 0;
            var yPos = 0;
            while (el) {
                if (el.tagName === "BODY") {
                    var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
                    var yScroll = el.scrollTop || document.documentElement.scrollTop;
                    xPos += (el.offsetLeft - xScroll + el.clientLeft);
                    yPos += (el.offsetTop - yScroll + el.clientTop);
                } else {
                    xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
                    yPos += (el.offsetTop - el.scrollTop + el.clientTop);
                }
                el = el.offsetParent;
            }
            return {
                x: xPos,
                y: yPos
            };
        },

        receiveMessage: function (callback, source_origin) {

            if (window['postMessage']) {
                if (callback) {
                    attached_callback = function (e) {
                        if ((typeof source_origin === 'string' && e.origin !== source_origin)
                            || (Object.prototype.toString.call(source_origin) === "[object Function]" && source_origin(e.origin) === !1)) {
                            return !1;
                        }
                        callback(e);
                    };
                }
                if (window['addEventListener']) {
                    window[callback ? 'addEventListener' : 'removeEventListener']('message', attached_callback, !1);
                } else {
                    window[callback ? 'attachEvent' : 'detachEvent']('onmessage', attached_callback);
                }
            } else {
                interval_id && clearInterval(interval_id);
                interval_id = 0;

                if (callback) {
                    interval_id = setInterval(function () {
                        var hash = document.location.hash,
                            re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({data: hash.replace(re, '')});
                        }
                    }, 100);
                }
            }
        }
    };
}();










(function createWidget() {

    var Util = {
        extendObject: function (a, b) {
            for (var prop in b) {
                if (b.hasOwnProperty(prop) && a.hasOwnProperty(prop)) {
                    a[prop] = b[prop];
                }
            }
            return a;
        },
        proto: 'https:' === document.location.protocol ? 'https://' : 'http://'
    };


    var obj = document.getElementById("fitnesskit_widget_script");
    
        var d=new Date(), now=new Date();
        d.setDate(now.getDate()- (now.getDay()+6)%7);
        var startDateStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");
        d.setDate(d.getDate()+6);
        var endDateStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-");

    
    

    var endDateStr = [d.getFullYear(),d.getMonth() + 1,d.getDate()].join("-"),
        clubId = obj.getAttribute('data-id'),
        domain = obj.getAttribute('data-domain'),
        path = obj.getAttribute('data-path'),
        container = obj.getAttribute('data-container')?obj.getAttribute('data-container'):"fitnesskit";
    widget_url = (document.location.protocol == 'https:' ? 'https:' : 'http:')+'//'+domain+path+'?start_date='+startDateStr+'&end_date='+endDateStr+'&club_id='+clubId;

    var Widget = {
        created: false,
        show: function () {
            if (this.created)
                return;
            this.widgetElement = document.createElement('div');
            this.widgetElement.setAttribute('id', 'fitnesskit_widget_container');
            this.widgetElement.innerHTML = '<iframe id="fitnesskit_widget_iframe" src="' + widget_url + '" scrolling="no"  style="width: 1px; min-width: 100%; *width: 100%;" width="100%" height="200" frameborder="0"></iframe>';
            document.getElementById(container).innerHTML = this.widgetElement.innerHTML;
            this.widgetElement.style.display = 'block';
            this.created = true;
        }
    };

  window.addEventListener("scroll", function() {

        var top = XD.getPosition(document.getElementById("fitnesskit_widget_iframe"))['y'];
        document.getElementById('fitnesskit_widget_iframe').contentWindow.postMessage(top, '*'); 
     }, false);
  window.addEventListener('message', function(event) {
    document.getElementById("fitnesskit_widget_iframe").height = event.data;
});
    Widget.show();
})();





