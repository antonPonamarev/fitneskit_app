 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">

<head>    
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Купить искуственную елку</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />   
    <link type="text/css" rel="stylesheet" media="all" href="/styles.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/device.css" />
    <link rel="stylesheet" href="/fancybox.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


    <meta name="description" content="У нас вы можете купить искуственную елку с гарантией лучшей цены. Все искуственные елки только выского качества."> 
    <meta name="Keywords" content="купить искуственную елку">  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>







    <script type="text/javascript" src="js/script.js"></script>   
	<script type="text/javascript" src="js/device.js"></script> 
   <script src="js/fancybox.js"></script>
    <script type="text/javascript" src="assets/countdown/jquery.countdown.js"></script>
  


</head>
<body>

<div id="fitneskit"></div>




<script type="text/javascript">
(function () {
  var f = function () {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.setAttribute('id', 'fitnesskit_widget_script'); //id элемента iframe 
    script.setAttribute('data-domain', 'fitnesskit.ponamarev-web.ru'); //домен
    script.setAttribute('data-id', '1'); //id клиента
    script.setAttribute('data-path', '/fitneskit/'); //путь к виджету на сервере 
    script.setAttribute('data-container', 'fitneskit'); //id контейнера, в который поместим виджет 
    script.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//fitnesskit.ponamarev-web.ru/fitneskit/mount/widget.js'; //скрипт, который инициирует работу виджета (промежуточное звено)
    document.getElementsByTagName('head')[0].appendChild(script);
  }
  if (window.opera == '[object Opera]') {
    document.addEventListener('DOMContentLoaded', f, false);
  } else { f(); }
 })()
</script>
</body>
 
</html>
